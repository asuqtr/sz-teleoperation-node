#!/usr/bin/env python
import rospy
from std_msgs.msg import Int16
from sensor_msgs.msg import Joy

motor1_msg = Int16()
motor2_msg = Int16()
motor3_msg = Int16()
motor4_msg = Int16()
servo1_msg = Int16()
servo2_msg = Int16()
servo3_msg = Int16()
servo4_msg = Int16()

servo1_minvalue = 100
servo1_maxvalue = 500
servo1_increment = 5
servo2_minvalue = 100
servo2_maxvalue = 500
servo2_increment = 5
servo3_opened_position = 100
servo3_closed_position = 200
servo4_list = [100, 200, 300, 400, 500, 600]

motor1_publisher = rospy.Publisher('motor1', Int16, queue_size=10) # From 0 to 4095
motor2_publisher = rospy.Publisher('motor2', Int16, queue_size=10) # From 0 to 4095
servo1_publisher = rospy.Publisher('servo1', Int16, queue_size=10) # From 0 to 4095
servo2_publisher = rospy.Publisher('servo2', Int16, queue_size=10) # From 0 to 4095
servo3_publisher = rospy.Publisher('servo3', Int16, queue_size=10) # From 0 to 4095
servo4_publisher = rospy.Publisher('servo4', Int16, queue_size=10) # From 0 to 4095

motor1_previousState = 0
motor2_previousState = 0
servo1_previousState = servo1_minvalue
servo2_previousState = servo2_minvalue
servo3_previousState = servo3_opened_position
servo4_previousIndex = servo4_list[0]

servo3_list = [100, 200]
servo3_last_index = 0
servo3_direction = 0

joystick_previous_state = Joy()
joystick_previous_state.buttons = [0] *10


def map(x, in_min, in_max, out_min, out_max):
    return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min


def callback(data):

    global joystick_previous_state

    # Motor 1
    motor1_msg.data = map(data.axes[1], -1, 1, -1023, 1023)
    global motor1_previousState
    if motor1_previousState != motor1_msg.data:
        motor1_publisher.publish(motor1_msg)        
        motor1_previousState = motor1_msg.data
    
    # Motor 2
    motor2_msg.data = map(data.axes[4], -1, 1, -1023, 1023)
    global motor2_previousState
    if motor2_previousState != motor2_msg.data:
        motor2_publisher.publish(motor2_msg)        
        motor2_previousState = motor2_msg.data
    
    # Servo 1
    servo1_movement = data.axes[7]
    global servo1_previousState
    if servo1_movement == 1:         
        if (servo1_previousState + servo1_increment) <= servo1_maxvalue:
            servo1_value = servo1_previousState + servo1_increment
            servo1_msg.data = servo1_value
            servo1_publisher.publish(servo1_msg)
            servo1_previousState = servo1_value
    if servo1_movement == -1:         
        if (servo1_previousState - servo1_increment) >= servo1_minvalue:
            servo1_value = servo1_previousState - servo1_increment
            servo1_msg.data = servo1_value
            servo1_publisher.publish(servo1_msg)
            servo1_previousState = servo1_value
    
    # Servo 2
    servo2_movement = data.axes[6] * -1
    global servo2_previousState
    if servo2_movement == 1:         
        if (servo2_previousState + servo2_increment) <= servo2_maxvalue:
            servo2_value = servo2_previousState + servo2_increment
            servo2_msg.data = servo2_value
            servo2_publisher.publish(servo2_msg)
            servo2_previousState = servo2_value
    if servo2_movement == -1:         
        if (servo2_previousState - servo2_increment) >= servo2_minvalue:
            servo2_value = servo2_previousState - servo2_increment
            servo2_msg.data = servo2_value
            servo2_publisher.publish(servo2_msg)
            servo2_previousState = servo2_value
    
    # Servo 3
    # global servo3_last_index

    if joystick_previous_state.buttons[0] != data.buttons[0] and data.buttons[0] == 1:
        rospy.loginfo("Event")

    # global btnA_previousState
    # global servo3_previousState
    # if data.buttons[0] != btnA_previousState and data.buttons[0] == 1:
    #     if servo3_previousState == servo3_opened_position:
    #         servo3_msg.data = servo3_closed_position
    #     if servo3_previousState == servo3_closed_position:
    #         servo3_msg.data = servo3_opened_position
    #     servo3_previousState = servo3_msg.data
    #     servo3_publisher.publish(servo3_msg)
    #     btnA_previousState = data.buttons[0]    
    
    # Servo 4
    servo4_msg.data = 0
    servo4_publisher.publish(servo4_msg)

    joystick_previous_state = data

    
def listener():
    rospy.init_node('teleop_node', anonymous=True)
    rospy.Subscriber('joy', Joy, callback)
    rospy.spin()

if __name__ == '__main__':
    listener()
